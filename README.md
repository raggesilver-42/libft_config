# libftconfig

This is a small program that allows easy "removal" of unused files from libft.

## Install
```bash
# this script uses async/await and therefore needs a somewhat recent version
# of Node.Js (>= 10.x)?
npm i # to install missing dependencies (there should be none)
```

## Usage
```
node libftconfig [cmd] [args]

cmds:
    enable [patterns]:  enable all files following the given patterns. If no
                        pattern is provided all disabled patterns are cleared
    disable [patterns]: disable files with the given pattern. If no pattern is
                        provided nothing happens
    set-path <path>:    sets the path of your libft. Must be a valid existing
                        directory
    make:               applies settings to the project

note: no settings will be applied to the project if you don't run node
      libftconfig make after making changes
```

## Note
This script should only be used with [my libft](https://gitlab.com/raggesilver-42/libft) **and only if all changes were committed, preferably pushed**!

## Todo
1. Ensure `*.not_c` files are `.gitignore`'ed
2. Ensure `libft`'s `.git` is deleted and submodules are removed (for vogsphere reasons)
3. 1 and 2 conflict...
