const fs = require('fs');
const path = require('path');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const settingsFile = path.join(__dirname, '.settings.json');

let settings;

function saveSettings() {
    fs.writeFileSync(settingsFile, JSON.stringify(settings));
}

async function make() {
    var src = path.join(settings.path, 'src');
    let cmds = [
        `for f in $(find ${src} -type f -name "*.not_c"); do
            mv "$f" "$(echo "$f" | sed s/not_c/c/)";
        done`,
        `make -C ${settings.path} -j`
    ];

    let { err } = await exec(cmds[0]);
    if (err) { console.error(err); process.exit(1); }

    for (const d of settings.disabled) {
        let { err, stdout } = await exec(`
            for f in $(find ${src} -type f -name "${d}"); do
                mv "$f" \${f%.*}.not_c;
            done`);
        console.log(stdout);
        if (err) { console.error(err); process.exit(1); }
    }

    err = (await exec(cmds[1])).err;
    if (err) { console.error(err); process.exit(1); }
}

if (fs.existsSync(settingsFile)) {
    settings = JSON.parse(fs.readFileSync(settingsFile));
} else {
    settings = {
        disabled: [],
        path: null
    };
    saveSettings();
}

if (process.argv.length > 2) {
    switch (process.argv[2]) {
        case "enable":
            if (process.argv.length == 3) {
                settings.disabled = [];
            } else {
                for (let i = 3; i < process.argv.length; i++) {
                    let ind = settings.disabled.indexOf(process.argv[i]);
                    if (ind != -1) {
                        settings.disabled.splice(ind, 1);
                    }
                }
            }
            saveSettings();
            break;
        case "disable":
            for (let i = 3; i < process.argv.length; i++) {
                let ind = settings.disabled.indexOf(process.argv[i]);
                if (ind == -1) {
                    settings.disabled.push(process.argv[i]);
                }
            }
            saveSettings();
            break;
        case "set-path":
            if (process.argv.length != 4) {
                console.error(
                    `Error: invalid number for arguments for set-path`);
                process.exit(1);
            }
            var p = process.argv[3];
            if (fs.existsSync(p) && fs.lstatSync(p).isDirectory()) {
                settings.path = path.resolve(p);
            } else {
                console.error(`Error: invalid path "${p}"`);
                process.exit(1);
            }
            saveSettings();
            break;
        case "make":
            if (!settings.path) {
                console.error(`Error: no path set. See libftconfig set-path`);
                process.exit(1);
            }
            try {
                make();
            } catch(e) {
                console.error(e);
            }
            break;
        default:
            console.error(`Error: unknown option "${process.argv[2]}"`);
            process.exit(1);
    }
}
